import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:parlimentmobile/alerts/CustomAlert.dart';
import 'package:parlimentmobile/config/const.dart';
import 'package:parlimentmobile/db/entity/note.dart';
import 'package:parlimentmobile/screens/dashboard.dart';
import 'package:parlimentmobile/screens/note_editor.dart';
import 'package:parlimentmobile/screens/note_updater.dart';
import 'package:parlimentmobile/util/toast.dart';
import 'package:path_provider/path_provider.dart';
import 'package:quill_delta/quill_delta.dart';
import 'package:zefyr/zefyr.dart';


// ignore: must_be_immutable
class NoteList extends StatelessWidget {
  CustomAlert customAlert = new CustomAlert();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Parliament_Mobile',
      home: MyNoteList(),
    );
  }
}






class MyNoteList extends StatefulWidget {
  MyNoteList({Key key}) : super(key: key);

  @override
  _MyNoteListState createState() => _MyNoteListState();
}


class _MyNoteListState extends State<MyNoteList> {
  final List<String> entries = [];
  final List<Note> notes = [];

  getNotes() async {
    Const.APPDATABASE.noteDao.findAllNotes().then((value) {
      setState(() {
        for(var note in value){
          notes.add(note);
          entries.add(""+note.title);
        }

      });
    });
  }


  @override
  initState(){
    super.initState();
    getNotes();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () =>  Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                  builder: (BuildContext context) => Dashboard()
              )
          ),
        ),
        title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Note Editor',
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          GestureDetector(
            child: Text('Create new ',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: Colors.white)),
          )
        ]),
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              icon: Icon(Icons.add),
              onPressed: () =>  Navigator.of(context).pushReplacement(
                  MaterialPageRoute(
                      builder: (BuildContext context) => NoteEditor()
                  )
              ),
            ),
          )
        ],
        backgroundColor: Colors.red[900],

      ),

      body: Padding(
        padding: EdgeInsets.all(20.0),
        child:
        ListView.builder(

            padding: const EdgeInsets.all(8),
            itemCount: notes.length,
            itemBuilder: (BuildContext context, int index) {
              return
                Container(
                  padding: EdgeInsets.fromLTRB(0,0,0,0),
                  height: 220,
                  width: double.maxFinite,
                  child: Card(
                    elevation: 5,
                    child:  Padding(
                        padding: const EdgeInsets.only(left: 10, top: 10),
                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(left: 10, top: 10),
                                  child:
                                  Row(
                                    children: <Widget>[

                                      Align(
                                          alignment: Alignment.centerLeft,
                                          child: Icon(
                                            Icons.event_note,
                                            color: Colors.amber,
                                            size: 40,
                                          )),
                                      SizedBox(width: 10,),
                                      Container(
                                        child: OutlineButton(
                                          child: Text(''+notes[index].id.toString()+' - ${notes[index].title}'),
                                          onPressed: () {
                                            toastSuccess("Open to Update - "+notes[index].id.toString());
                                            Const.NOTEID = notes[index].id;
                                            Const.NOTE_SELECTED = notes[index];
                                            Navigator.of(context).pushReplacement(
                                                MaterialPageRoute(builder: (BuildContext context) => NoteUpdater()));
                                          },
                                        ),
                                      ),
                                      
                                    ],
                                  )
                                )
                              ],
                            )
                          ],
                        )),
                  ),
                );
            }
        )


      ),
    );
  }


}




