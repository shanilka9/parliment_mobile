import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shimmer/shimmer.dart';
import 'package:parlimentmobile/screens/dashboard.dart';


class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState(){
    super.initState();

    _mockCheckForSession().then(
            (status) {
          if (status) {
            _navigateToHome();
          } else {
            _navigateToLogin();
          }
        }
    );
  }


  Future<bool> _mockCheckForSession() async {
    await Future.delayed(Duration(milliseconds: 4000), () {});
    return true;
  }

  void _navigateToHome(){
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(
            builder: (BuildContext context) => Dashboard()
        )
    );
  }

  void _navigateToLogin(){
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(
            builder: (BuildContext context) => Dashboard()
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/logo.png',
                height: 80,
                fit:BoxFit.fitHeight),
            Shimmer.fromColors(
              period: Duration(milliseconds: 1000),
              baseColor: Colors.brown,
              highlightColor: Colors.red,
              child: Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  "e-Parliament",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'Pacifico',
                      shadows: <Shadow>[
                        Shadow(
                            blurRadius: 18.0,
                            color: Colors.black87,
                            offset: Offset.fromDirection(120, 12)
                        )
                      ]
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.center,
//              child: Text("1.0.0v",
//                style: TextStyle(
//                    fontSize: 9,
//                    fontFamily: 'Pacifico',
//                    shadows: <Shadow>[
//                      Shadow(
//                          blurRadius: 18.0,
//                          color: Colors.black87,
//                          offset: Offset.fromDirection(120, 12)
//                      )
//                    ]
//                ),
//              ),
            ),
          ],
        ),
      )
    );
  }


}