import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:parlimentmobile/alerts/CustomAlert.dart';
import 'package:parlimentmobile/config/const.dart';
import 'package:parlimentmobile/db/entity/note.dart';
import 'package:parlimentmobile/screens/note_list.dart';
import 'package:path_provider/path_provider.dart';
import 'package:quill_delta/quill_delta.dart';
import 'package:zefyr/zefyr.dart';

// ignore: must_be_immutable
class NoteEditor extends StatelessWidget {
  CustomAlert customAlert = new CustomAlert();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Parliament_Mobile',
      home: MyNoteEditor(),
    );
  }
}

class MyNoteEditor extends StatefulWidget {
  MyNoteEditor({Key key}) : super(key: key);

  @override
  _MyNoteEditorState createState() => _MyNoteEditorState();
}

class _MyNoteEditorState extends State<MyNoteEditor> {
  int currentIndex = 0;
  final controller = ScrollController();
  var noteTitleTxt = TextEditingController();
  ZefyrController _zefyrController;
  FocusNode _focusNode;
  var systempath = "";
  String message = '';

  Future<NotusDocument> _loadDocument() async {
   /* final file = File(Directory.systemTemp.path + "/quick_start.json");
    if (await file.exists()) {
      final contents = await file.readAsString();
      return NotusDocument.fromJson(jsonDecode(contents));
    }*/
    final Delta delta = Delta()..insert(" \n");
    return NotusDocument.fromDelta(delta);
  }

  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    systempath = directory.path;
    print(directory.path);
    return directory.path;
  }

  void _saveDocument(BuildContext context) {
/*    _localPath;
    final contents = jsonEncode(_noteDescriptionTxt.document);
//    final file = File(systempath + "/quick_start.json");
    final file = File(Directory.systemTemp.path + "/quick_start.json");
    file.writeAsString(contents).then((_) {
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Saved."+_noteDescriptionTxt.document.toJson().toString())));
    });*/

    if(noteTitleTxt.value.text.trim().length == 0){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Cannot be empty Title"))); return;
    }
    else if(_zefyrController.document.toJson().toString().trim().length == 0){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("Cannot be empty Description"))); return;
    } else {
      saveNote("" + noteTitleTxt.value.text,
          "" + _zefyrController.document.toJson().toString());
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (BuildContext context) => NoteList()));
    }
  }

  int saveNote(String title, String content) {
    int retVal = 0;
    Note note = new Note(null, title, content);
    Const.APPDATABASE.noteDao.insertNote(note).then((value) {
      retVal = value;
    });
    return retVal;
  }

  @override
  initState() {
    super.initState();
    _loadDocument().then((document) {
      setState(() {
        _zefyrController = ZefyrController(document);
      });
    });
    _focusNode = FocusNode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () => Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (BuildContext context) => NoteList())),
        ),
        title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Note Editor',
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          GestureDetector(
            child: Text('Create new ',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: Colors.white)),
          )
        ]),
        actions: <Widget>[
          Builder(
            builder: (context) => IconButton(
              icon: Icon(Icons.save),
              onPressed: () => _saveDocument(context),
            ),
          )
        ],
        backgroundColor: Colors.red[900],
      ),
      body: Padding(
        padding: EdgeInsets.all(20.0),
        child:
//        NoteForm(hasEditNote, editNote),
            /* Markdown(
          controller: controller,
          selectable: true,
          data: _markdownData,
          imageDirectory: 'https://raw.githubusercontent.com',
        ),*/

            ZefyrScaffold(
          child: ListView(
            children: <Widget>[
              TextField(
                  controller: noteTitleTxt,
                  decoration: InputDecoration(labelText: 'Title')),
              ZefyrField(
                height: MediaQuery.of(context).size.height,
                decoration: InputDecoration(labelText: 'Description'),
                controller: _zefyrController,
                focusNode: _focusNode,
                autofocus: false,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
