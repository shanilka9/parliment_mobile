import 'package:flutter/material.dart';
import 'package:parlimentmobile/alerts/CustomAlert.dart';
import 'package:parlimentmobile/db/database.dart';
import 'package:parlimentmobile/screens/note_editor.dart';
import 'package:parlimentmobile/screens/note_list.dart';

// ignore: must_be_immutable
class Dashboard extends StatelessWidget {
  CustomAlert customAlert = new CustomAlert();


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Parliament_Mobile',
      home: MyHome(),
    );
  }
}

class MyHome extends StatefulWidget {
  MyHome({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyHome> {
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('THE PARLIAMENT',
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.white)),
          GestureDetector(
            child: Text('OF SRI LANKA',
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.normal,
                    color: Colors.white)),
          )
        ]),
        backgroundColor: Colors.red[900],
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
              size: 20,
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.computer,
              color: Colors.white,
              size: 20,
            ),
          )
        ],
      ),
      body: ListView(
        padding: const EdgeInsets.all(8),
        children: <Widget>[
          Container(
            height: 200,
            color: Colors.white,
            child: const Center(child: Text('Tile A')),
          ),
          Container(
            height: 200,
            color: Colors.white,
            child: const Center(child: Text('Tile B')),
          ),
          Container(
            height: 200,
            color: Colors.white,
            child: const Center(child: Text('Tile C')),
          ),
          Container(
            height: 200,
            color: Colors.white,
            child: const Center(child: Text('Tile D')),
          ),
          Container(
            height: 200,
            color: Colors.white,
            child: const Center(child: Text('Tile E')),
          ),
        ],
      ),
      drawer: Drawer(
          child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountEmail: Text('Epic Lanka(PVT)Ltd.'),
            accountName:
            Text("E-Parliament", style: TextStyle(fontSize: 18)),
            currentAccountPicture: CircleAvatar(
              maxRadius: 200,
              minRadius: 200,
              backgroundImage: ExactAssetImage('assets/images/gov.png'),
              backgroundColor: Colors.transparent,
            ),
            margin: const EdgeInsets.only(bottom: 0),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  // Colors are easy thanks to Flutter's Colors class.
                  Colors.red[800],
                  Colors.red[700],
                  Colors.red[600],
                  Colors.red[400],
                  Colors.red[300],
                ],
              ),
            ),
          ),

          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.home, size: 24, color: Colors.black54),
                    Text("\t Home",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.access_time, size: 24, color: Colors.black54),
                    Text("\t Schedule",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.description,
                        size: 24, color: Colors.black54),
                    Text("\t Requests",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.comment, size: 24, color: Colors.black54),
                    Text("\t Speeches",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.chrome_reader_mode, size: 24, color: Colors.black54),
                    Text("\t News",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.event_note, size: 24, color: Colors.black54),
                    Text("\t Upcoming Events ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.shop,
                        size: 24, color: Colors.black54),
                    Text("\t Hansard ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.people,
                        size: 24, color: Colors.black54),
                    Text("\t Members of Parliament ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.view_list,
                        size: 24, color: Colors.black54),
                    Text("\t My Attendance ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.inbox, size: 24, color: Colors.black54),
                    Text("\t My Votes ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.local_activity, size: 24, color: Colors.black54),
                    Text("\t My Parliament Activities ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 0, top: 15),
            child: Divider(),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.settings, size: 24, color: Colors.black54),
                    Text("\t Settings ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 25, top: 15),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(children: <Widget>[
                    Icon(Icons.exit_to_app, size: 24, color: Colors.black54),
                    Text("\t Logout ",
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        ))
                  ])),
            ),
          ),
        ],
      )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                  builder: (BuildContext context) => NoteList()
              )
          );
        },
        child: Icon(Icons.note_add),
        backgroundColor: Colors.red[900],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home', style: TextStyle(fontSize: 10.0)),
              backgroundColor: Colors.red[900]),
          BottomNavigationBarItem(
              icon: Icon(Icons.chrome_reader_mode),
              title: Text('Hansard', style: TextStyle(fontSize: 10.0)),
              backgroundColor: Colors.red[900]),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Text(
              'Calendar',
              style: TextStyle(fontSize: 10.0),
            ),
            backgroundColor: Colors.red[900],
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.speaker_notes),
            title: Text(
              'Speeches',
              style: TextStyle(fontSize: 10.0),
            ),
            backgroundColor: Colors.red[900],
          )
        ],
        onTap: (index) {
          setState(() {
            currentIndex = index;
          });
        },
      ),
    );
  }
}
