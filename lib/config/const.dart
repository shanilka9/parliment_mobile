import 'package:parlimentmobile/db/database.dart';
import 'package:parlimentmobile/db/entity/note.dart';

class Const {
  static String KEY_BASE_URL = "";
  static AppDatabase APPDATABASE = null;
  static int NOTEID = null;
  static Note NOTE_SELECTED = null;
}
