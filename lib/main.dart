
import 'package:flutter/material.dart';
import 'package:parlimentmobile/config/const.dart';
import 'package:parlimentmobile/screens/dashboard.dart';
import 'package:parlimentmobile/screens/note_editor.dart';
import 'package:parlimentmobile/screens/note_list.dart';
import 'package:parlimentmobile/screens/note_updater.dart';
import 'package:parlimentmobile/screens/splash_screen.dart';

import 'db/database.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final database =
      await $FloorAppDatabase.databaseBuilder('app_database.db').build();
  Const.APPDATABASE = database;
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    title: 'Named Routes Demo',
    initialRoute: 'splash',
    routes: {

      'splash': (context) => SplashScreen(),
      'dashboard': (context) => Dashboard(),
      'note_editor': (context) => NoteEditor(),
      'note_updater': (context) => NoteUpdater(),
      'note_list': (context) => NoteList()
    },
  ));
}


