import 'package:flutter/material.dart';

class CustomAlert {
  void defaultAlert(BuildContext context, String alertTitle, String content) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(alertTitle),
              titleTextStyle: TextStyle(
                  color: Colors.purple, fontSize: 16, fontWeight: FontWeight.bold),
              content: Text(content),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
              elevation: 8.0,
              actions: <Widget>[
                new FlatButton(
                  child: new Text("CANCEL"),
                  textColor: Colors.red,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                new FlatButton(
                  child: new Text("ACCEPT"),
                  textColor: Colors.green,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ]);
        });
  }
}