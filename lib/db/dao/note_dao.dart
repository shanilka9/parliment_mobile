import 'package:parlimentmobile/db/entity/note.dart';
import 'package:floor/floor.dart';


@dao
abstract class NoteDao {


  @Query('SELECT * FROM Note')
  Future<List<Note>> findAllNotes();

  @Query('SELECT * FROM Note')
  Stream<List<Note>> findAllNotesAsStream();

  @Query('SELECT * FROM Note WHERE id = :id')
  Future<Note> findNotesById(int id);

  @Query('SELECT * FROM Note WHERE Note.un = :un AND Note.pw = :pw')
  Future<Note> findNoteByUnPw(String un, String pw);

  @Query('SELECT * FROM Note WHERE id = (SELECT MAX(id) FROM Note)')
  Future<Note> findNoteMaxId();

  @update
  Future<int> updateNote(Note note);

  @insert
  Future<int> insertNote(Note note);

  @delete
  Future<int> removeNote(Note note);
}