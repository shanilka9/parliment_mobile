import 'dart:async';
import 'package:floor/floor.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart' as sqflite;
import 'dao/note_dao.dart';
import 'entity/note.dart';

part 'database.g.dart'; //run cmd: flutter packages pub run build_runner build


@Database(version: 4, entities: [Note])
abstract class AppDatabase extends FloorDatabase {
  NoteDao get noteDao;
//  UserDao get userDao;
}
